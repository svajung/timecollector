#!/usr/bin/env python3
__author__ = 'svantje'
__version__ = 1.4


import argparse
import glob
import os
from colorama import init, Fore, Back, Style
from datetime import datetime
from datetime import timedelta
from pathlib import Path


class Timekeeper:
    items = list()
    filename = "tmp-times.csv"


class Item:
    def __init__(self, date, start, end, title):
        self.start = start
        self.end = end
        self.duration = self.end - self.start
        self.date = date
        self.title = title
        self.paused = self.start - self.start
        self.pause_start = self.start
        self.pause_end = self.start
        self.active = True

    def set_message(self, new_message):
        self.title = new_message

    def set_end(self, end):
        self.end = end
        self.duration = self.end - self.start - self.paused

    def set_date(self, date):
        self.date = date

    def set_pause_start(self, start):
        self.pause_start = start
        self.set_end(start)
        self.active = False

    def set_pause_end(self, end):
        self.paused = self.paused + (end - self.pause_start)
        self.active = True

    def get_date(self):
        return self.date

    def get_duration(self):
        return self.duration

    def get_start(self):
        return self.start

    def get_end(self):
        return self.end

    def get_title(self):
        return self.title

    def get_paused(self):
        return  self.paused

    def is_active(self):
        return self.active


def main():
    global items, filename

    init(autoreset=True)
    parser = argparse.ArgumentParser()
    parser.add_argument("-date", "--date", help="Date of item",
                        type=lambda d: datetime.strptime(d, '%d.%m.%Y').date())
    parser.add_argument("-d", "--duration", help="Duration of entry",
                        type=lambda d: datetime.strptime(d, '%H:%M'))
    parser.add_argument("-u", "--unknown", help="Do not assume timestamp on adding todays tasks with duration",
                        action='store_true')

    parser.add_argument("-f", "--file", help="Read from file")
    parser.add_argument("-m", "--msg", help="Item description")

    parser.add_argument("-in", "--checkin", help="Check in", action='store_true')
    parser.add_argument("-out", "--checkout", help="Check out", action='store_true')
    parser.add_argument("-t", "--time", help="Check in/out timestamp",
                        type=lambda t: datetime.strptime(t, '%H:%M'))
    args = parser.parse_args()

    if args.date:
        date = args.date
        date = date.strftime('%d.%m.%Y')
    else:
        date = datetime.now().strftime('%d.%m.%Y')

    if args.file:
        filename = args.file
    else:
        files = glob.glob("./*-times.csv")
        if len(files) > 1:
            # too many possible files
            print("Please specify the file the tasks should be written to. "
                  "Found multiple possibilities (files ending with '-times.csv')")
            return
        elif len(files) == 1:
            # use existing file
            filename = files[0]
        else:
            # save in new file, labeled with current working directory
            filename = os.path.basename(os.getcwd()) + "-times.csv"

    print("\nWriting to '" + filename + "'")

    # Process check-in and check-out
    if args.checkin:
        if args.time:
            ts = args.time
        else:
            ts = datetime.strptime(datetime.now().strftime('%H:%M:%S'), '%H:%M:%S')
        write_to_csv(filename, Item(date, ts, ts, "Arrived"))
        print("\n" +
              Back.WHITE + Fore.BLACK + " Started working at " +
              Style.RESET_ALL + ts.strftime(' %H:%M:%S'))
        return
    elif args.checkout:
        if args.time:
            ts = args.time
        else:
            ts = datetime.strptime(datetime.now().strftime('%H:%M:%S'), '%H:%M:%S')
        write_to_csv(filename, Item(date, ts, ts, "Departed"))
        print("\n" +
              Back.WHITE + Fore.BLACK + " Ended working at " +
              Style.RESET_ALL + ts.strftime(' %H:%M:%S'))
        return

    if not args.msg:
        args.msg = input("\n" + Back.CYAN + Fore.BLACK + " Name of task " + Style.RESET_ALL + "\n>>> ")

    # Start time tracking
    if args.duration:
        if date == datetime.now().strftime('%d.%m.%Y') and not args.unknown:
            # if tracked for current day, assume now is end time of task
            ended = datetime.now()
            hours, minutes = args.duration.hour, args.duration.minute
            started = ended - timedelta(hours=hours, minutes=minutes)
        else:
            started = datetime.strptime("00:00", "%H:%M")
            ended = args.duration
        job = Item(date, started, ended, args.msg)
        write_to_csv(filename, job)
    elif date == datetime.now().strftime('%d.%m.%Y'):
        # mode: False = paused, True = running
        active = True

        # no duration given
        start = datetime.now()
        #tmp_duration = start - start


        job = Item(date, start, start, args.msg)
        Timekeeper.items.append(job)

        help_msg()

        print("\n" + Back.CYAN + Fore.BLACK + " Name of task " + Style.RESET_ALL + " '" + job.get_title() + "'")
        print("\n" +
              Back.GREEN + Fore.BLACK + " Started " +
              Style.RESET_ALL + " tracking time: ",
              start.strftime('%d.%m.%Y - %H:%M:%S'))

        while True:
            putin = input()
            #-----------------------------------------------------------------#
            # Abort job (not saving to file)
            #-----------------------------------------------------------------#
            if putin == 'abort' or putin == 'a':
                job = abort(job)
                if job == None:
                    return

            #-----------------------------------------------------------------#
            # End job
            #-----------------------------------------------------------------#
            elif putin == 'end' or putin == 'e' or putin == 'q' or putin == 'quit':
                job = quit(job)
                if job == None:
                    return

            # -----------------------------------------------------------------#
            # End job and resume
            # -----------------------------------------------------------------#
            elif putin == 'er':
                job = quit(job)
                if job == None:
                    return
                job = resume(job)

            # -----------------------------------------------------------------#
            # End job and start new one
            # -----------------------------------------------------------------#
            elif putin == 'en':
                _ = quit(job)
                job = push(None, date)

            #-----------------------------------------------------------------#
            # Pause job
            #-----------------------------------------------------------------#
            elif putin == 'p' or putin == 'pause':
                job = pause(job)

            #-----------------------------------------------------------------#
            # Resume job
            #-----------------------------------------------------------------#
            elif putin == 'r' or putin == 'resume':
                job = resume(job)

            #-----------------------------------------------------------------#
            # Stack new job
            #-----------------------------------------------------------------#
            elif putin == 'n' or putin == 'new':
                job = push(job, date)

            # -----------------------------------------------------------------#
            # Info about current jobs
            # -----------------------------------------------------------------#
            elif putin == 'i' or putin == 'info':
                job = info(job)

            # -----------------------------------------------------------------#
            # Change current job title
            # -----------------------------------------------------------------#
            elif putin == 'c' or putin == 'change':
                print(">>> New title:")
                new_title = input()
                job.set_message(new_title)

            #-----------------------------------------------------------------#
            # Else
            #-----------------------------------------------------------------#
            else:
                print("Unknown command.")
                help_msg()
    else:
        print("Can only track time on today's date")
        return


def abort(job):
    job.set_end(datetime.now())
    print("\n" + Back.WHITE + Fore.BLACK + " Aborted " + Style.RESET_ALL + " time tracking of job '" +
          job.get_title() + "' with current duration of " + str(job.get_duration()).split(".")[0] + " hours \n")
    Timekeeper.items.pop()
    if len(Timekeeper.items) == 0:
        return None
    else:
        job = Timekeeper.items[-1]
        paused_at(job)
        return job


def quit(job):
    end = datetime.now()
    if not job.is_active():
        job.set_pause_end(end)

    job.set_end(end)

    print("\n" +
          Back.RED + Fore.BLACK + " Ended " +
          Style.RESET_ALL + print_job(job))

    # write to csv
    write_to_csv(filename, job)
    Timekeeper.items.pop()

    if len(Timekeeper.items) == 0:
        return None
    else:
        job = Timekeeper.items[-1]
        paused_at(job)
        return job


def paused_at(job):
    print(">>> Currently " +
          Back.YELLOW + Fore.BLACK + " paused " + Style.RESET_ALL +
          " at '" + job.get_title() + "'")


def pause(job):
    if not job.is_active():
        print(">>> Already paused!")
    else:
        pause = datetime.now()
        job.set_pause_start(pause)

        print("\n" +
          Back.YELLOW + Fore.BLACK + " Paused: " +
          Style.RESET_ALL,
          pause.strftime('%H:%M:%S'),
          "\tCurrent duration: ", str(job.get_duration()).split(".")[0]
          )

    return job


def resume(job):
    if not job.is_active():
        pause_end = datetime.now()
        job.set_pause_end(pause_end)

        print("\n" +
              Back.GREEN + Fore.BLACK + " Resumed: " + Style.RESET_ALL,
              pause_end.strftime('%H:%M:%S'))
    else:
        print(">>> Task is already active!")

    return job


def push(job, date):
    if job != None and job.is_active():
        # pause current job
        job.set_pause_start(datetime.now())

    message = input("\n" + Back.CYAN + Fore.BLACK + " Name of task " + Style.RESET_ALL + "\n>>> ")

    new_start = datetime.now()
    new_job = Item(date, new_start, new_start, message)
    Timekeeper.items.append(new_job)
    job = Timekeeper.items[-1]

    print("\n" +
          Back.GREEN + Fore.BLACK + " Started " +
          Style.RESET_ALL + " tracking time: ",
          new_start.strftime('%d.%m.%Y - %H:%M:%S'))

    return job


def info(job):
    if job.active:
        job.set_end(datetime.now())
    else:
        job.set_end(job.pause_start)
    print("\n>>> " +
          Back.CYAN + Fore.BLACK + " Current " + Style.RESET_ALL + " '" +
          job.get_title() + "': " +
          " duration: " + str(job.get_duration()).split(".")[0])

    for item in range(len(Timekeeper.items) - 2, -1, -1):
        print(">>> " + Back.YELLOW + Fore.BLACK + " Paused " + Style.RESET_ALL + " '" +
              Timekeeper.items[item].get_title() + "': " +
              " duration: " + str(Timekeeper.items[item].get_duration()).split(".")[0])
    print()
    return job


def print_job(job):
    return " task '" + job.get_title() \
           + "' | start: " + str(job.get_start().strftime("%H:%M:%S")) \
           + " | end: " + str(job.get_end().strftime("%H:%M:%S")) \
           + " | paused: " + str(job.get_paused()).split(".")[0] \
           + " | duration: " + str(job.get_duration()).split(".")[0] \
           + " | date: " + str(job.get_date())


def write_to_csv(filename, item):
    if not Path(filename).exists():
        with open(filename, "w") as csv:
            csv.write("year,month,day,start,end,paused,duration,message\n")
    with open(filename, "a") as csv:
        date = str(item.get_date()).split(".")
        csv.write(date[2] + "," + date[1] + "," + date[0] + "," +
                  str(item.get_start().strftime("%H:%M:%S")) + "," +
                  str(item.get_end().strftime("%H:%M:%S")) + "," +
                  str(item.get_paused()).split(".")[0] + "," +
                  str(item.get_duration()).split(".")[0] + "," +
                  item.get_title() + "\n")


def help_msg():
    print(">>> enter 'a[bort]' to abort time tracking\n"
          ">>> enter 'c[hange]' to rename current tasktitle\n"
          ">>> enter 'e[nd]' or 'q[uit]' to finish tracking + saving track to db\n"
          ">>> enter 'i[nfo]' to get info about all stacked tasks\n"
          ">>> enter 'n[ew]' to start a new task (stacked tasks)\n"
          ">>> enter 'p[ause]' to pause time tracking\n"
          ">>> enter 'r[esume]' to resume time tracking\n"
          ">>> enter 'en' to end current task and create a new task\n"
          ">>> enter 'er' to end current task and directly resume stacked task")


def pre_main():
    try:
        main()
    except KeyboardInterrupt:
        print()
        for it in Timekeeper.items:
            end = datetime.now()
            if not it.is_active():
                it.set_pause_end(end)
            it.set_end(end)

            print(Back.WHITE + Fore.BLACK + " Ended " + Style.RESET_ALL + " time tracking of job '" +
                  it.get_title() + "' with current duration of " + str(it.get_duration()).split(".")[0] + " hours.\n")
            write_to_csv(Timekeeper.filename, it)


if __name__ == '__main__':
    pre_main()
