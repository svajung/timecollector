# timecollector

Timecollector is a small, interactively usable CLI time-tracking tool.
Useful for memorizing hours worked on specific tasks for university or on the job.
On using interactive tracking the tasks can be aborted, paused, stacked and ended.
Finished tasks are written to a CSV-file in the current working-directory or a given path.

For summary purposes and an easy to read overview of the tracked time, there is an additional script that generates a Markdown file from the given CSV-file.

## Prerequisites
+ python3
+ python3-pip

```
# for interactive time-tracking
pip3 install colorama
```

## Usage
If you want to track time from and in any directory, add an alias to your bashrc/zshrc/...
Be aware that the program will try to write to a CSV-file ending on `*-times.csv`.
Using other file names or when encountering multiple files ending on `*-times.csv` in the directory, you will have to set the correct filepath with every command manually.
```
# example alias
alias tc="<path/to/timecollector>/timecollector.py"
alias tcr="<path/to/timecollector>/report.py"
```

## Examples for time tracking
```
# Start interactive time-tracking, writing to '<foldername>-times.csv'
tc -m 'Research on ..'

# Adding finished task with a duration in hours:minutes
tc -m 'Meeting' -d 0:15

# Adding task that was finished another day (dd.mm.yyyy)
tc -m 'Meeting' -d 0:15 -date 01.04.2042
```
### Switches during active time tracking
For interactively time-tracking there are some options to abort, rename, pause, resume and end the current task.
As stacking of tasks is possible, you can display information on all of them.

| switch | description |
| :-----: | ------ |
| a | abort time tracking |
| c | rename current tasktitle | 
| e | finish tracking + saving track to db |
| q | finish tracking + saving track to db |
| i | get info about all stacked tasks |
| n | start a new task (stacked tasks)|
| p | pause time tracking |
| r | resume time tracking |
| en | end current task and create a new task |
| er | end current task and directly resume stacked task |

## Keeping track on checking in to/ out from work
These entries will be listed separately in a report.
For the moment they are not considered in other respects.

```
# Check in (today, just now)
tc -in

# Check in (other day, specific time)
tc -in -date 05.06.2019 -t 8:05

# Check out (today, specific time)
tc -out -t 18:05
```

## Examples for generating time reports
The reporting tool will collapse tasks with the same description that occured on the same day.
Output file is a Markdown file.
You can use a Mardown (pre-)viewer or convert to PDF for improved readability.

```
# Generate report for '<foldername>-times.csv'
tcr

# Generate report for 'work-times.csv'
tcr -f work-times.csv

# Generate report for single month (e.g. June)
tcr -m 6
```
