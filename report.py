#!/usr/bin/env python3
__author__ = 'svantje'
__version__ = 1.6

import argparse
import calendar
from calendar import Calendar
import csv
import datetime
from datetime import timedelta
import glob
import locale

try:
    import holidays
except ImportError:
    print('The "holidays" package is missing, install with python-pip:')
    print('  $ sudo python3 -m pip install holidays')
    exit(1)

class Item:
    def __init__(self, year, month, day, duration, note):
        self.day = "-".join([str(year),str(month),str(day)])
        h, m, s = [int(i) for i in duration.split(':')]
        self.duration = datetime.timedelta(seconds=3600*h + 60*m + s)
        self.note = note

    def __str__(self):
        return ",".join([self.day, str(self.duration), self.note])

    def duration_to_decimal(self):
        return self.duration.total_seconds() / 3600.0

    def duration(self):
        return self.duration

    def print(self):
        return self.day + ": " + self.note + " : " + str(round(self.duration_to_decimal(), 2)) + " [h]"

    def print_without_date(self):
        return str(round(self.duration_to_decimal(), 2)) + " (" + str(self.duration) + ") [h]" + " : " + self.note

    def update_duration(self, additional):
        self.duration = self.duration + additional
        return self


class CheckItem:
    def __init__(self, year, month, day, time, note):
        self.day = "-".join([str(year), str(month), str(day)])
        self.note = note
        self.time = time
        self.duration = datetime.timedelta()

    def print_without_date(self):
        return "> " + self.note + " : " + self.time

    def update_duration(self, additional):
        return self

    def duration_to_decimal(self):
        return self.duration.total_seconds() / 3600.0


def get_item(row):
    if not row[0].isdigit():
        return None
    if row[7] == "Arrived" or row[7] == "Departed":
        return CheckItem(row[0], row[1], row[2], row[3], row[7])
    return Item(row[0], row[1], row[2], row[6], row[7])


def collect_data(filename, year, month, short):
    days = dict()

    with open(filename, mode='r') as csvfile:
        times = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in times:
            if row[0] != year:
                continue

            if month != "-1" and row[1] != month:
                continue

            item = get_item(row)
            if item is None:
                continue

            if item.day in days :
                # day already in dictionary
                if short:
                    project = item.note.split(" ")[0]
                    if project in days[item.day]:
                        days[item.day][project] = days[item.day][project].update_duration(item.duration)
                    else:
                        item.note = project
                        days[item.day][project] = item
                else:
                    if item.note in days[item.day]:
                        # day with same note already in dictionary - update duration
                        days[item.day][item.note] = days[item.day].get(item.note).update_duration(item.duration)
                    else:
                        # new task encountered
                        days[item.day][item.note] = item
            else:
                # add day to dictionary
                days[item.day] = {item.note : item}

    return days


def write_markdown(filename, days):
    with open(filename, mode='w') as md:
        year = 0
        month = 0
        month_total = 0.0
        day_total = 0.0

        for k in sorted(days.keys()):
            y, m, d = k.split("-")

            if y != year:
                year = y
                md.write("# {}\n".format(year))

            if m != month:
                md.write(end_of_month(int(year), int(month), month_total))
                month_total = 0
                month = m
                md.write("## {}\n".format(calendar.month_name[int(month)]))

            md.write("### {}.{}.{}\n```\n".format(d,m,y))
            lines_day = list()
            id = 0
            arrived = None
            departed = None
            for v in sorted(days[k]):
                day_total += days[k][v].duration_to_decimal()
                if v == "Arrived":
                    arrived = days[k][v].print_without_date()
                    lines_day.insert(id, arrived + "\n")
                    id += 1
                elif v == "Departed":
                    departed = days[k][v].print_without_date()
                    lines_day.insert(id, departed + "\n")
                    id += 1
                else:
                    lines_day.append(days[k][v].print_without_date() + "\n")

            md.writelines(lines_day)
            md.write("\n")

            hours_at_work = ""
            if arrived is not None and departed is not None:
                split_arrived = arrived.split(":")
                split_departed = departed.split(":")
                t1 = timedelta(hours=int(split_arrived[1]), minutes=int(split_arrived[2]), seconds=int(split_arrived[3]))
                t2 = timedelta(hours=int(split_departed[1]), minutes=int(split_departed[2]), seconds=int(split_departed[3]))
                hours_at_work_time = t2 - t1
                hours_at_work = " (A-D: {} Stunden)\n".format(round(hours_at_work_time.total_seconds() / 3600.0, 2))

            if day_total < 8.0:
                md.write("\u24cd {} Stunden{}\n".format(round(day_total, 2), hours_at_work))
            else:
                md.write("\u2705 {} Stunden{}\n".format(round(day_total, 2), hours_at_work))

            month_total += day_total
            day_total = 0

            md.write("```\n")

        md.write(end_of_month(int(year), int(month), month_total))


def end_of_month(year, month, total_hours):
    out = ""
    if total_hours > 0:
        total_workdays, workdays_passed = number_of_workdays(year, month, datetime.date.today())
        worked = "Aktuell: {} Stunden".format(round(total_hours, 2))
        quota = ""
        if month == datetime.datetime.now().month and total_workdays != workdays_passed:
            quota = "{} Stunden ({}d)".format(workdays_passed * 8, workdays_passed)
        overall = "{} Stunden ({}d)".format(total_workdays * 8, total_workdays)
        out = "```\n{} / {} / {}\n```\n---\n".format(worked, quota, overall)
    return out


def number_of_workdays(year, month, today):
    number_of_workdays = 0
    workdays = list()
    calendar = Calendar()
    dates = calendar.itermonthdates(year, month)

    for date in dates:
        if date.month == month and date.weekday() < 5 and date not in holidays.DE(state='NI', years=date.year):
            number_of_workdays += 1
            workdays.append(date)

    days_worked = 0
    for wd in workdays:
        if wd <= today:
            days_worked += 1

    return number_of_workdays, days_worked


def main():
    """
    # Year
    ## Month
    ### Date
    X Duration - Task
    [ ] Day Duration - Start - End - Pause
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="Read from file")
    parser.add_argument("-m", "--month", help="Show results only for this month")
    parser.add_argument("-y", "--year", type=int, help="Show results only for this year", default=datetime.datetime.now().year)
    parser.add_argument('--short', dest='short', action='store_true')
    parser.add_argument("--pdf", help="Generate pdf output")
    args = parser.parse_args()

    # argument handling
    if args.file:
        filename = args.file
    else:
        files = glob.glob("./*-times.csv")
        if len(files) > 1:
            # too many possible files
            print("Please specify the file the tasks should be written to. "
                  "Found multiple possibilities (files ending with '-times.csv')")
            return
        elif len(files) == 1:
            # use existing file
            filename = files[0]
        else:
            raise FileNotFoundError

    month = "-1"
    if args.month:
        month = str(args.month).zfill(2)

    year = str(args.year)

    # collect tasks and times
    days = collect_data(filename, year, month, args.short)

    # write summary as markdown
    if len(days) == 0:
        print("No items to write for current selection.")
        return

    output_file = filename.split(".csv")[0]
    if month != "-1":
        output_file += "-" + year + "-" + month
    output_file += ".md"

    print("Writing summary of times to {}".format(output_file))
    write_markdown(output_file, days)

    # convert to pdf
    if args.pdf:
        print("Not yet implemented.")
        raise NotImplementedError


if __name__ == '__main__':
    locale.setlocale(locale.LC_ALL, '')
    main()
