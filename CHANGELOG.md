# report
## v1.6
+ allow the report to be shortened by topic
  + assuming topic/project is the first keyword in description
  + all times with keyword are summarized
  + allows to commit the times

## v1.5
+ output filename contains both year and month, instead of only the month

## v1.4
+ when report is generated for current month the total hours, that should be reached by the end of the current day, are displayed

## v1.3
+ adds number of workdays for each month
+ adds total time (departure - arrival time) for day, if both values are present

# timecollector
## v1.4
+ adds start and endtime when adding items on the same day with duration
  + assumes endtime of task is the current timestamp
+ assumption on start and endtime can be prevented by adding argument `-u`

## v1.3
+ fixes the displayed duration when querying information on a paused job

## v1.2
+ added possibility to set a start/arrived or end/departed time on any day